// SSL Integration
const express = require('express');
// const https = require('https');
const http = require('http');

const fs = require('fs');
const path = require('path');
const ffmpeg = require('fluent-ffmpeg');

const app = express();
const server = http.createServer(app);
const cors = require('cors');

// Middleware to parse JSON bodies
app.use(express.json());

const rtspToPNG = (rtspAddress) => {
    return new Promise((resolve, reject) => {
      console.log('RTSP to PNG conversion start...');
      
      const outputFilePath = path.join(__dirname, 'captures', `output-${new Date().toISOString()}.png`);
      
      ffmpeg(rtspAddress)
        .addOption('-vframes', 1)
        .addOption('-q:v', 2)
        .save(outputFilePath)
        .on('progress', function(info) {
          console.log('progress ' + info.percent + '%');
        })
        .on('end', function() {
          console.log('End event..');
          resolve(outputFilePath);
        })
        .on('codecData', function(data) {
          console.log('Input is ' + data.audio + ' audio ' +
            'with ' + data.video + ' video');
        })
        .on('error', function(err) {
          console.log('An error happened: ' + err.message);
          reject(err);
        });
  
    });
};
  
// Example usage:
// rtspToPNG('rtsp://34.122.63.116:8554/mansion-backyard')
//     .then((filePath) => {
//         console.log('File saved at:', filePath);
//     })
//     .catch((err) => {
//         console.error('Error during conversion:', err);
//     });

//setting middleware
app.use('/thumbnails', express.static(__dirname + '/captures')); //Serves resources from recordings folder

// Add headers before the routes are defined
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // res.setHeader('Access-Control-Allow-Origin', 'https://www.barrier.technology');
  
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  
  // Pass to next layer of middleware
  next();
});

// this is an example html page to view the stream
app.post('/thumbnail', (req, res) => {
  console.log('New Thumbnail creation..');
  console.log('RTSP address used: ', req.body.rtspAddress);

  rtspToPNG(req.body.rtspAddress)
    .then((filePath) => {
        console.log('File saved at:', filePath);
        console.log('File basename:', path.basename(filePath));
        res.json({
          imageUrl: path.basename(filePath)
        });
    })
    .catch((err) => {
        console.error('Error during conversion:', err);
        res.status(500);
    });

});

app.get('/isOnline', (req, res) => {
  console.log('Online Check');
  res.status(200).json({ isOnline: true });
});

server.listen(5100, () => {
  console.log(`Secure SSL/TSL Server Started on port 5000!`);
});