const fs = require('fs');
const ffmpeg = require('fluent-ffmpeg');
const { createCanvas, loadImage } = require('canvas');

const cocoSsd = require('@tensorflow-models/coco-ssd');
require('@tensorflow/tfjs-backend-cpu');
require('@tensorflow/tfjs-backend-webgl');

// Path to the HLS manifest file
const manifestPath = 'http://192.168.1.101:5000/footage/home-cam/mystream.m3u8';

// Path to the output video file
const outputVideoPath = 'output/output.mp4';

// Initialize COCO-SSD model
console.log('Loading cocoSSD...');
let model;
cocoSsd.load().then((loadedModel) => {
  console.log('CocoSSD Loaded...');
  model = loadedModel;

  // Start processing HLS stream
  processHLSStream();
});

// Function to process HLS stream
function processHLSStream() {
    console.log('Processing stream...');
  // Create a writable stream for the output video
  var outputStream = fs.createWriteStream(outputVideoPath);

  // Use fluent-ffmpeg to process HLS stream and apply object detection
  ffmpeg(manifestPath)
    // .input(manifestPath)
    // .inputOptions('-protocol_whitelist', 'file,http,https,tcp,tls')
    .videoCodec('copy') // Copy video codec to avoid re-encoding
    .audioCodec('copy') // Copy audio codec to avoid re-encoding
    .save('output/drone-saved.mp4')
    .on('start', (commandLine) => {
        console.log('Processing started.');
        console.log('Spawned Ffmpeg with command: ' + commandLine);
    })
    .on('end', () => {
      console.log('Processing complete.');
    })
    .on('error', (err) => {
      console.error('Error:', err);
    })
    .on('video', async (frame) => {
        // Process the frame with COCO-SSD
        const detections = await detectObjects(frame);

        // Log or use the detected objects as needed
        console.log('Detected Objects:', detections);
    });
    // .pipe(outputStream, { end: true });

  // Add event listener for each video frame
  ffmpeg().input(manifestPath)
    .inputOptions('-protocol_whitelist', 'file,http,https,tcp,tls')
    .on('video', async (frame) => {
        // Process the frame with COCO-SSD
        const detections = await detectObjects(frame);

        // Log or use the detected objects as needed
        console.log('Detected Objects:', detections);
    });
}

// Function to perform object detection using COCO-SSD
async function detectObjects(frame) {
  // Load the frame as an image using canvas
  const img = await loadImage(frame);

  // Create a canvas and draw the image on it
  const canvas = createCanvas(img.width, img.height);
  const ctx = canvas.getContext('2d');
  ctx.drawImage(img, 0, 0, img.width, img.height);

  // Use COCO-SSD to detect objects in the frame
  const detections = await model.detect(canvas);

  return detections;
}
