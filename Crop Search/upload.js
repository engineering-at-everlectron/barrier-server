import { GoogleVertexAIMultimodalEmbeddings } from "langchain/experimental/multimodal_embeddings/googlevertexai";
import { FaissStore } from "@langchain/community/vectorstores/faiss";
import { Document } from "@langchain/core/documents";

// import { MatchingEngine } from "langchain/vectorstores/googlevertexai";

import express from 'express';
import bodyParser from 'body-parser';

const app = express();
const PORT = 3550;

const embeddings = new GoogleVertexAIMultimodalEmbeddings();

// Loading existing vectorstor
const vectorStore = await FaissStore.load("./", embeddings);

// const vectorStore = await FaissStore.fromTexts(
//   ["truck", "fedex"],
//   [{ id: 1 }, { id: 3 }],
//   embeddings
// );

// Middleware to parse JSON bodies
app.use(bodyParser.json());

// Express route to handle base64 string and object
app.post('/upload', async (req, res) => {
    const { base64String, id, mediaType } = req.body;

    // You can perform further processing with the received data here
    console.log('Received base64 string:', base64String);
    console.log('Received id:', id);
    console.log('Received mediaType:', mediaType);

    const vectors = await embeddings.embedImageQuery(base64String);

    const document = new Document({
        pageContent: base64String,
        // Metadata is optional but helps track what kind of document is being retrieved
        metadata: {
            id,
            mediaType,
        },
    });

    // Add the image embedding vectors to the vector store directly
    await vectorStore.addVectors([vectors], [document]);

    // Save the state of the vectorStore
    await vectorStore.save("./");

    // Example response
    res.status(200).json({ message: 'Image embedded successfully!' });
});

// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
