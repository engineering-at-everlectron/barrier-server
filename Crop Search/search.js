import fs from "fs";
import path from 'path';
import { GoogleVertexAIMultimodalEmbeddings } from "langchain/experimental/multimodal_embeddings/googlevertexai";

import { FaissStore } from "@langchain/community/vectorstores/faiss";
import { Document } from "@langchain/core/documents";
import cors from 'cors';
// import { MatchingEngine } from "langchain/vectorstores/googlevertexai";

import express from 'express';
import bodyParser from 'body-parser';

const app = express();
const PORT = 3555;

const embeddings = new GoogleVertexAIMultimodalEmbeddings();

// Loading existing vectorstor
// const vectorStore = await FaissStore.load("./", embeddings);

// Loading existing vectorstor
const vectorStore = await FaissStore.load("./", embeddings);

// const vectorStore = await FaissStore.fromTexts(
//   ["car", "truck", "fedex"],
//   [{ id: 2 }, { id: 1 }, { id: 3 }],
//   embeddings
// );

// Middleware to parse JSON bodies
// app.use(bodyParser.json());
app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ limit: '10mb', extended: true }));
app.use(express.text({ limit: '10mb' }));

// Allow requests from all domains
app.use(cors());

// Express route to handle base64 string and object
app.post('/search', async (req, res) => {
    const base64String = req.body.base64String;
    console.log('Req Body Base64: ', req.body.base64String);

    // You can perform further processing with the received data here
    console.log('Received base64 string:', base64String);
    const vectors = await embeddings.embedImageQuery(base64String);

    // Use the lower level, direct API
    const resultTwo = await vectorStore.similaritySearchVectorWithScore(
        vectors,
        3
    );
    console.log(JSON.stringify(resultTwo, null, 2));

    // Example response
    res.status(200).json({ message: 'Search successfull!', searchResult: resultTwo });
});

// Start the server
app.listen(PORT, () => {
    console.log(`CropSearch Server is running on port ${PORT}`);
});
