const { spawn } = require('child_process');
const fs = require('fs');
const tf = require('@tensorflow/tfjs-node'); // Example for TensorFlow.js
// Set up the object detection model here

let recording = false;

const ffmpeg = spawn('ffmpeg', ['-i', '<video_source>', '-vf', 'fps=1', '-f', 'image2pipe', '-vcodec', 'mjpeg', '-q:v', '1', '-']);
ffmpeg.stdout.on('data', async (frame) => {
    const detection = await detectObject(frame); // A function that detects objects in a frame

    if (detection && !recording) {
        recording = true;
        startRecording(); // Trigger a 15-second recording
        setTimeout(() => { recording = false; }, 15000); // Reset after 15 seconds
    }
});

async function detectObject(frame) {
    // Object detection logic using a pre-trained model
    const image = tf.node.decodeImage(frame);
    const predictions = await model.detect(image); // Apply your object detection here
    image.dispose();

    return predictions.some(pred => pred.class === 'desiredObject'); // Example condition for detection
}

function startRecording() {
    const output = `output_${Date.now()}.mp4`;
    const record = spawn('ffmpeg', ['-i', '<video_source>', '-t', '15', '-c:v', 'copy', output]);

    record.on('close', (code) => {
        console.log(`Recording finished with code ${code}`);
    });
}
