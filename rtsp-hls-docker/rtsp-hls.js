const express = require('express');
const http = require('http');
const app = express();

const server = http.createServer(app);
const ffmpeg = require('fluent-ffmpeg');
const cors = require('cors');

const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue } = require('firebase-admin/firestore');
const serviceAccount = require('./barrier-34a55-7b27ad6f0902.json');

const rtspToHLS = (rtspInput) => {
    console.log('Stream Input: ', rtspInput);
    console.log('RTSP to HLS stream start...');

    ffmpeg(rtspInput)
        .audioCodec('aac')
        .audioBitrate('160000')
        .audioChannels(2)
        .size('854x480')
        .videoCodec('libx264')
        .videoBitrate(800000)
        .addOption('-hls_time', 5)
        .addOption('-hls_list_size', 5)
        .addOption('-start_number', 5)
        .save(__dirname + '/recordings/mystream.m3u8')
        .on('progress', function(info) {
            console.log('progress ' + info.percent + '%');
        })
        .on('end', function() {
            console.log('End event..');
        })
        .on('codecData', function(data) {
            console.log('Input is ' + data.audio + ' audio ' +
            'with ' + data.video + ' video');
        })
        .on('error', function(err) {
            console.log('an error happened: ' + err.message);
        });
};

initializeApp({
  credential: cert(serviceAccount)
});

const db = getFirestore();

// Localstorage
if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./container');
}

if(localStorage.getItem('containerId') === null) {
    let conData = {
        name: 'Change my name. EG: cam 1 ',
        type: 'RTSP-HLS',
        facilityId: 'Change my facility ID',
        rtspInput: null,
        createdAt: new Date()
    };

    db.collection('containers').add(conData).then(res => {
        localStorage.setItem('containerId', res.id);
        console.log('Container added: ', res.id);
        startSubscription(res.id);
    }).catch(err => console.log(err));
} else {
    startSubscription(localStorage.getItem('containerId'));
}

function startSubscription(containerId) {
    const conDoc = db.collection('containers').doc(containerId);

    conDoc.onSnapshot(docSnapshot => {
        // console.log(`Received doc snapshot: ${docSnapshot}`);
        console.log(`Data: ${JSON.stringify(docSnapshot.data())}`);
        const data = JSON.stringify(docSnapshot.data());

        if (data.rtspInput === null) {
            console.log('No Input.');
            return;
        } else if (data.rtspInput != null && localStorage.getItem('rtspInput') === null) {
            localStorage.setItem('rtspInput', data.rtspInput);
            rtspToHLS(rtspInput);
        } else if (data.rtspInput != localStorage.getItem('rtspInput')) {
            localStorage.setItem('rtspInput', data.rtspInput);
            ffmpeg.exit();
            rtspToHLS(rtspInput);
        } else if (data.rtspInput === localStorage.getItem('rtspInput') && localStorage.getItem('rtspInput') != null) {
            console.log('No updates to RTSP link..');
            return;
        }
    }, err => {
        console.log(`Encountered error: ${err}`);
    });
}

// Add headers before the routes are defined
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');
    // res.setHeader('Access-Control-Allow-Origin', 'https://www.barrier.technology');
    
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    
    // Pass to next layer of middleware
    next();
});

app.get('/', (req, res) => {
    console.log('Get Root Page');
  
    res.send(`
      <h1>Serving to you, from Barrier HQ!</h1>
    `)
});

app.get('/isOnline', (req, res) => {
    console.log('Online Check');
    res.status(200).json({ isOnline: true });
});

server.listen(4000, () => {
    console.log(`Secure SSL/TSL Server Started on port 4000!`);
});