const { initializeApp, applicationDefault, cert } = require('firebase-admin/app');
const { getFirestore, Timestamp, FieldValue } = require('firebase-admin/firestore');
const http = require('http');

const serviceAccount = require('./barrier-34a55-7b27ad6f0902.json');

initializeApp({
  credential: cert(serviceAccount)
});

const db = getFirestore();

// Localstorage
if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./node');
}

if(localStorage.getItem('nodeId') === null) {
    let nodeData = {
        name: 'Change my name',
        facilityId: 'Change my facility ID',
        createdAt: new Date(),
        publicIp: null
    };

    db.collection('nodes').add(nodeData).then(res => {
        localStorage.setItem('nodeId', res.id);
        console.log('Node added: ', res.id);
        startSubscription(res.id);
    }).catch(err => console.log(err));
} else {
    startSubscription(localStorage.getItem('nodeId'));
}

function startSubscription(nodeId) {
    updateIP();
    setInterval(() => {
        updateIP();
    }, 30000);

    const nodeDoc = db.collection('nodes').doc(nodeId);

    nodeDoc.onSnapshot(docSnapshot => {
        // console.log(`Received doc snapshot: ${docSnapshot}`);
        console.log(`Data: ${JSON.stringify(docSnapshot.data())}`);
        // ...
    }, err => {
        console.log(`Encountered error: ${err}`);
    });
}

// Public IP
function updateIP() {
    const nodeId = localStorage.getItem('nodeId');
    console.log('IP Update Check: ', new Date());

    http.get({'host': 'api.ipify.org', 'port': 80, 'path': '/'}, function(resp) {
        resp.on('data', function(ip) {

            console.log("My public IP address is: " + ip);
            // console.log('IP type: ', typeof(ip));
    
            if(localStorage.getItem('publicIp') === null && nodeId != null) {
    
                localStorage.setItem('publicIp', `${ip}`);
                db.collection('nodes').doc(nodeId).update({
                    publicIp: `${ip}`
                }).then(res => console.log('Node PublicIP Set.')).catch(err => console.log(err));
    
            } else if (localStorage.getItem('publicIp') === ip && nodeId != null) {
                console.log('Public IP Update check. No changes');
                return;
    
            } else if (localStorage.getItem('publicIp') != ip && nodeId != null) {
    
                localStorage.setItem('publicIp', `${ip}`);
                db.collection('nodes').doc(nodeId).update({
                    publicIp: `${ip}`
                }).then(res => console.log('Node Document Updated.')).catch(err => console.log(err));
    
            }
    
    
        });
    });
}
