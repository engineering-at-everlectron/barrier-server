import '@tensorflow/tfjs-backend-cpu';
import '@tensorflow/tfjs-backend-webgl';

import { ChatGoogleGenerativeAI } from "@langchain/google-genai";
import fs from 'fs';
import { HumanMessage } from "langchain/schema";

import { Firestore } from '@google-cloud/firestore';

// Read JSON file
const readJSONFile = (filename) => {
    try {
        const data = fs.readFileSync(filename, 'utf8');
        console.log('Got JSON Stack: ', data);
        return JSON.parse(data);
    } catch (err) {
        console.error('Error reading JSON file:', err);
        return [];
    }
}

// Write JSON file
function writeJSONFile(filename, data) {
    try {
        fs.writeFileSync(filename, JSON.stringify(data, null, 2), 'utf8');
    } catch (err) {
        console.error('Error writing JSON file:', err);
    }
}

let camera;
let content = [
  {
    type: "text",
    text: "Pretend that you are a cctv operator monitoring human/vehicle related footage for any anomalies or threats. The ideal output should contain a json output that rates the level of severity from 1-5, provides a caption, a Boolean indicating suspicious activity, a Boolean indicating fire detected, a Boolean indicating smoke detected, any liscence plates and total humans and vehicles in a provided image or images. Here are the image/images:",
  }
];

let stack = readJSONFile('stack.json');

// Load Gemini Pro Vision
const visionModel = new ChatGoogleGenerativeAI({
  model: "gemini-pro-vision",
  maxOutputTokens: 2048,
});
console.log('Gemini Loaded');

// Load Firestore & Subscribe to single camera document
// Get camera document ID from eventArc cloudEvents metadata

// Create a new client
const firestore = new Firestore();
console.log('Firestore Loaded');
console.log('Loading complete');

const start = async () => {
  // Obtain a document reference.
  const document = firestore.doc('cameras/Eo5gOmupkOKj2bJdyBvz');

  // Read the document.
  const doc = await document.get();
  console.log('Read the document: ', doc._fieldsProto);
  camera = doc._fieldsProto;

  // Check if operatorGPT should be ran
  if (camera.operatorGPT.booleanValue) {
    runOperatorGPT();
    console.log('OperatorGPT enabled');
  } else {
    console.log('OperatorGPT not enabled');
  }

  // Enter new data into the document.
  // await document.set({
  //   title: 'Welcome to Firestore',
  //   body: 'Hello World',
  // });
  // console.log('Entered new data into the document');

  // Update an existing document.
  // await document.update({
  //   body: 'My first Firestore app',
  // });
  // console.log('Updated an existing document');

  // Delete the document.
  // await document.delete();
  // console.log('Deleted the document');
}
start();

const fileExists = (filePath) => {
    try {
        fs.accessSync(filePath, fs.constants.F_OK);
        return true; // File exists
    } catch (err) {
        return false; // File does not exist
    }
}

const imageToBase64 = (filePath) => {
  // Read the image file
  const imageData = fs.readFileSync(filePath);

  // Convert image data to base64 format
  const base64String = Buffer.from(imageData).toString('base64');

  return base64String;
}

const clearImageStack = () => {
    stack.forEach(img => deleteImageByPath(img));
    stack = [];
    console.log('Stack cleared: ', stack);

    writeJSONFile('stack.json', stack);
}

const deleteImageByPath = async (filePath) => {
  fs.unlink(filePath, (err) => {
    if (err) {
      console.error(`Error deleting file: ${err}`);
      return;
    }
    // console.log(`File ${filePath} has been deleted successfully.`);
  });
}

// OperatorGPT function 
// Only run OperatorGPT if Camera.operatorGPT = true
const runOperatorGPT = async () => {
  console.log('OperatorGPT Triggered');
  console.log('Operator Interval set to 15s');

  setInterval(async () => {
    stack = readJSONFile('stack.json');
    console.log('OperatorGPT input image stack: ', stack);
  
    if(stack.length >= 1 ) {
      console.log('Calling OperatorGPT..');
      let counter = 0;

      stack.forEach(async (img) => {
        // Limit to 16 images from stack
        if(counter == 15) {
            return;
        }

        if (fileExists(img)) {
            counter++;
            let data = imageToBase64(img);
    
            // content = [...content, {
            //   type: "image_url",
            //   image_url: `data:image/png;base64,${data}`
            // }]
            content.push(
              {
                type: "image_url",
                image_url: `data:image/png;base64,${data}`
              }
            );
        }
      });
  
      // Theres a bug somewhere here thats not allowing image content 
      // to be pushed into the content array. What could it be?
    //   console.log('OperatorGPT Content: ', content);
    
      const input2 = [
        new HumanMessage({
          content
        }),
      ];

      console.log('Calling OperatorGPT.. ');
    
      // const res = await visionModel.invoke(input2);
      // console.log('OperatorGPT Response: ', res.content);

      visionModel.invoke(input2).then(res => {
        console.log('OperatorGPT Response: ', res.content);
        // Clear stack now that we have what we need in content []
        clearImageStack();

        // Work with the new response
        // Then add res to room chat message as OperatorGPT
      }).catch(err => console.log(err));

      // Decide whether or not to clear stack
    } else {
      console.log('Nothing in the stack. Not calling OperatorGPT');
    }
  }, 15000);

}
