import { GoogleVertexAIMultimodalEmbeddings } from "langchain/experimental/multimodal_embeddings/googlevertexai";
import { FaissStore } from "@langchain/community/vectorstores/faiss";

// import { MatchingEngine } from "langchain/vectorstores/googlevertexai";
const embeddings = new GoogleVertexAIMultimodalEmbeddings();

const vectorStore = await FaissStore.fromTexts(
  ["truck", "fedex"],
  [{ id: 1 }, { id: 3 }],
  embeddings
);

// Save the state of the vectorStore
await vectorStore.save("./");

console.log('VectorStore Created: ', vectorStore);