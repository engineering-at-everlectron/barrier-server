import { pipeline } from '@xenova/transformers';

let pipe = await pipeline('object-detection');
console.log('Image object detection pipeline loaded');

const result = await pipe('./car-fire.jpg', { topk: 6 });
console.log('Detection Result: ', result);