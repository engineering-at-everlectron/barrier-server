import '@tensorflow/tfjs-backend-cpu';
import '@tensorflow/tfjs-backend-webgl';

import { ChatGoogleGenerativeAI } from "@langchain/google-genai";
import fs from 'fs';
import { HumanMessage } from "langchain/schema";

import chokidar from 'chokidar';
import cocoSsd from '@tensorflow-models/coco-ssd';
import { exec } from 'child_process';

import { GoogleVertexAIMultimodalEmbeddings } from "langchain/experimental/multimodal_embeddings/googlevertexai";
import { FaissStore } from "@langchain/community/vectorstores/faiss";
import { Document } from "@langchain/core/documents";

import { Firestore } from '@google-cloud/firestore';
import { pipeline } from '@xenova/transformers';

// Read JSON file
function readJSONFile(filename) {
  try {
      const data = fs.readFileSync(filename, 'utf8');
      return JSON.parse(data);
  } catch (err) {
      console.error('Error reading JSON file:', err);
      return [];
  }
}

// Write JSON file
function writeJSONFile(filename, data) {
  try {
      fs.writeFileSync(filename, JSON.stringify(data, null, 2), 'utf8');
  } catch (err) {
      console.error('Error writing JSON file:', err);
  }
}

let camera;
// let content = [
//   {
//     type: "text",
//     text: "Pretend that you are a cctv operator monitoring human/vehicle related footage for any anomalies or threats. The ideal output should contain a json output that rates the level of severity from 1-5, provides a caption, a Boolean indicating suspicious activity, a Boolean indicating fire detected, a Boolean indicating smoke detected, any liscence plates and total humans and vehicles in a provided image or images. Here are the image/images:",
//   }
// ];

let stack = readJSONFile('stack.json');

// Load Gemini Pro Vision
const visionModel = new ChatGoogleGenerativeAI({
  model: "gemini-pro-vision",
  maxOutputTokens: 2048,
});
console.log('Gemini Loaded');

// Load EmbeddingsAPI and existing VectorStore
const embeddings = new GoogleVertexAIMultimodalEmbeddings();
const vectorStore = await FaissStore.load("./", embeddings);
console.log('VectorStore Loaded');

// Load CocoSSD Model
// const model = await cocoSsd.load();
// console.log('CocoSSD Model Loaded.');

// Load transformers image object detection model
let pipe = await pipeline('object-detection');
console.log('Image object detection pipeline loaded');

// Load Firestore & Subscribe to single camera document
// Get camera document ID from eventArc cloudEvents metadata

// Create a new client
const firestore = new Firestore();
console.log('Firestore Loaded');
console.log('Loading complete');

// Chokidar File Watcher for images
// File watcher should watch folder by camera id
// If there is none, it should create it
const watcher = chokidar.watch('images', {ignored: /^\./, persistent: true});

const startWatcher = () => {
  watcher
    .on('add', async (path) => {
      checkImage(path); // Kickoff
    })
    .on('change', function(path) {})
    .on('unlink', function(path) {})
    .on('error', function(error) {console.error('Error happened', error);})
}

const start = async () => {
  // start file watcher
  startWatcher();

  // Obtain a document reference.
  const document = firestore.doc('cameras/Eo5gOmupkOKj2bJdyBvz');

  // Read the document.
  const doc = await document.get();
  console.log('Read the document: ', doc._fieldsProto);
  camera = doc._fieldsProto;

  // If there is no HLS address ie no stream
  // Pass Camera to RTSP-PNG (The Trigger)
  rtspToPNG(camera);

  // Check if operatorGPT should be ran
  if (camera.operatorGPT.booleanValue) {
    // runOperatorGPT();
    console.log('OperatorGPT enabled');
  } else {
    console.log('OperatorGPT not enabled');
  }

  // Enter new data into the document.
  // await document.set({
  //   title: 'Welcome to Firestore',
  //   body: 'Hello World',
  // });
  // console.log('Entered new data into the document');

  // Update an existing document.
  // await document.update({
  //   body: 'My first Firestore app',
  // });
  // console.log('Updated an existing document');

  // Delete the document.
  // await document.delete();
  // console.log('Deleted the document');
}
start();

// Stop watching.
const stopWatcher = async () => {
  await watcher.close();
  console.log('Watcher stopped.')
}

// RTSP to PNG with FFMPEG
// Only run if camera.activityChecker = true
// Output images to folder by camera id
const rtspToPNG = (camera) => {
  console.log('Starting RTSP to PNG...');
  const command = `ffmpeg -f lavfi -i anullsrc -rtsp_transport udp -i "${camera.rtspAddress.stringValue}" -vf fps=1 ./images/out%d.png`;

  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`Error executing the command: ${error}`);
      return;
    }
    console.log(`Output: ${stdout}`);
    if(stderr){
      console.error(`Error: ${stderr}`);
    }
  });
}

const imageToBase64 = (filePath) => {
  // Read the image file
  const imageData = fs.readFileSync(filePath);

  // Convert image data to base64 format
  const base64String = Buffer.from(imageData).toString('base64');

  return base64String;
}

// Start/Stop HSL Stream
// Output stream files to folder by camera id
// Update HLS on camera document
const rtspToHLS = (camera) => {
  console.log('Starting RTSP to HLS...');
  const command = `ffmpeg -i "${camera.rtspAddress.stringValue}" -y -c:a aac -b:a 160000 -ac 2 -s 854x480 -c:v libx264 -b:v 800000 -hls_time 10 -hls_list_size 10 -start_number 1 ./streams/mystream.m3u8`;
  
  exec(command, (error, stdout, stderr) => {
    if (error) {
      console.error(`Error executing the command: ${error}`);
      return;
    }
    console.log(`Output: ${stdout}`);
    if(stderr){
      console.error(`Error: ${stderr}`);
    }
  });
}

// CocoSSD Human/Vehicle Check | Swap Coco model for something that works in Node
// Update Camera document human/vehicle counter
const checkImage = async (image) => {
  // Classify the image.
  const predictions = await pipe(image, { topk: 6 });

  // Check if there are humans or vehicles
  // Delete image if there is none
  // Function to check existence of 'car' or 'person' label in the object array
  const hasCarOrPerson = (objects) => {
    for (const obj of objects) {
        // Check if the label property exists and contains either 'car' or 'person'
        if (obj.label && (obj.label.toLowerCase() === 'car' || obj.label.toLowerCase() === 'person')) {
            return true; // Return true if found
        }
    }
    return false; // Return false if not found
  }

  // Check if the object array contains 'car' or 'person'
  const containsCarOrPerson = hasCarOrPerson(predictions);
  if (!containsCarOrPerson) {
    deleteImageByPath(image);
    console.log('No humans or vehicles detected..');
    
    // Stop Stream
  } else {
    console.log('Detected Humans or Vehicles!');
    addImagePathToStack(image);
    
    // Embed image to data lake
    // Start Stream if needed
  }

  // 30 second stack check
  // if (stack.length >= 30) {
  //   runOperatorGPT();
  // }

}

const addImagePathToStack = (image) => {
  stack.push(image);
  console.log('Image Path Added to stack: ', stack);
  create(image);
}

// const clearImageStack = () => {
//   stack.forEach(img => deleteImageByPath(img));
//   stack = [];
//   console.log('Stack cleared: ', stack);
// }

const deleteImageByPath = async (filePath) => {
  fs.unlink(filePath, (err) => {
    if (err) {
      console.error(`Error deleting file: ${err}`);
      return;
    }
    // console.log(`File ${filePath} has been deleted successfully.`);
  });
}

// Create Item in stack.json
const create = (obj) => {
  console.log(obj, ' added to stack.json');
  stack.push(obj);
  writeJSONFile('stack.json', stack);
}

// OperatorGPT function 
// Only run OperatorGPT if Camera.operatorGPT = true
// const runOperatorGPT = async () => {
//   console.log('OperatorGPT Triggered');
//   console.log('Operator Interval set to 15s');

//   setInterval(async () => {
  
//     if(stack.length >= 1 ) {
//       console.log('Calling OperatorGPT..');

//       stack.forEach(async img => {
//         console.log('Loop through stack');
//         let data = await imageToBase64(img);
//         content = [...content, {
//           type: "image_url",
//           image_url: `data:image/png;base64,${data}`
//         }]
//         // content.push(
//         //   {
//         //     type: "image_url",
//         //     image_url: `data:image/png;base64,${data}`
//         //   }
//         // );
//       });
  
//       // Theres a bug somewhere here thats not allowing image content 
//       // to be pushed into the content array. What could it be?
//       console.log('OperatorGPT Content: ', content);
    
//       const input2 = [
//         new HumanMessage({
//           content
//         }),
//       ];

//       console.log('Calling OperatorGPT.. ');
    
//       // const res = await visionModel.invoke(input2);
//       // console.log('OperatorGPT Response: ', res.content);

//       visionModel.invoke(input2).then(res => {
//         console.log('OperatorGPT Response: ', res.content);
//         // Clear stack now that we have what we need in content []
//         clearImageStack();

//         // Work with the new response
//       }).catch(err => console.log(err));

//       // Decide whether or not to clear stack
//     } else {
//       console.log('Nothing in the stack. Not calling OperatorGPT');
//     }


//   }, 15000);

// }

// Embed Image
// Only Embed Image If theres activity
const embedImage = async () => {
  const vectors = await embeddings.embedImageQuery(base64String);

  const document = new Document({
      pageContent: base64String,
      // Metadata is optional but helps track what kind of document is being retrieved
      metadata: {
          id,
          mediaType,
      },
  });

  // Add the image embedding vectors to the vector store directly
  await vectorStore.addVectors([vectors], [document]);

  // Save the state of the vectorStore
  await vectorStore.save("./");
}

// Then add res to room chat message as OperatorGPT