import { exec } from 'child_process';
import { Firestore } from '@google-cloud/firestore';

// const command = 'ffmpeg -f lavfi -i anullsrc -rtsp_transport udp -i "rtsp://barrierDemo:barrierDemo@192.168.1.108:554/cam/realmonitor?channel=1&subtype=1" -vf fps=1 ./images/out%d.png';
// exec(command, (error, stdout, stderr) => {
//   if (error) {
//     console.error(`Error executing the command: ${error}`);
//     return;
//   }
//   console.log(`Output: ${stdout}`);
//   if(stderr){
//     console.error(`Error: ${stderr}`);
//   }
// });

// Start/Stop HSL Stream
// const command = 'ffmpeg -i "rtsp://barrierDemo:barrierDemo@192.168.1.108:554/cam/realmonitor?channel=1&subtype=1" -y -c:a aac -b:a 160000 -ac 2 -s 854x480 -c:v libx264 -b:v 800000 -hls_time 10 -hls_list_size 10 -start_number 1 ./streams/mystream.m3u8';

// exec(command, (error, stdout, stderr) => {
//   if (error) {
//     console.error(`Error executing the command: ${error}`);
//     return;
//   }
//   console.log(`Output: ${stdout}`);
//   if(stderr){
//     console.error(`Error: ${stderr}`);
//   }
// });

// Create a new client
const firestore = new Firestore();

async function quickstart() {
  // Obtain a document reference.
  const document = firestore.doc('cameras/Eo5gOmupkOKj2bJdyBvz');

  // Read the document.
  const doc = await document.get();
  console.log('Read the document: ', doc._fieldsProto);

  // Enter new data into the document.
  // await document.set({
  //   title: 'Welcome to Firestore',
  //   body: 'Hello World',
  // });
  // console.log('Entered new data into the document');

  // Update an existing document.
  // await document.update({
  //   body: 'My first Firestore app',
  // });
  // console.log('Updated an existing document');

  // Delete the document.
  // await document.delete();
  // console.log('Deleted the document');
}
quickstart();