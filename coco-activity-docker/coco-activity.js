// const cocoSsd = require('@tensorflow-models/coco-ssd');
const chokidar = require('chokidar');
const watcher = chokidar.watch('captures', {ignored: /^\./, persistent: true});

// let model;
// cocoSsd.load().then(res => {
//     console.log('Model loaded');
//     model = res;
// }).catch(err => console.log(err));

watcher
  .on('add', function(path) {
    console.log('File', path, 'has been added');

    // Classify the image.
    // console.log('Starting Coco image analysis');
    // model.detect(image).then(res => console.log('Image results: ', res)).catch(err => console.log(err));
  })
  .on('change', function(path) {console.log('File', path, 'has been changed');})
  .on('unlink', function(path) {console.log('File', path, 'has been removed');})
  .on('error', function(error) {console.error('Error happened', error);})