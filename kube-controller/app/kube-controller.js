const k8s = require('@kubernetes/client-node');

// Create a Kubernetes API client using the configuration from your kubeconfig file:
const kc = new k8s.KubeConfig();
kc.loadFromDefault(); // Loads the configuration from the default kubeconfig file (~/.kube/config)
const k8sApi = kc.makeApiClient(k8s.AppsV1Api);

console.log('Controller Init..');

// Define the deployment manifest as an object:
const deploymentManifest = {
    apiVersion: 'apps/v1',
    kind: 'Deployment',
    metadata: {
      name: 'my-test-deployment',
    },
    spec: {
      replicas: 2,
      selector: {
        matchLabels: {
          app: 'my-test-hello-app',
        },
      },
      template: {
        metadata: {
          labels: {
            app: 'my-test-hello-app',
          },
        },
        spec: {
          containers: [
            {
              name: 'hello-world',
              image: 'hello-world',
              ports: [
                {
                  containerPort: 8080,
                },
              ],
            },
          ],
        },
      },
    },
  };

//Create the deployment:
k8sApi.createNamespacedDeployment('default', deploymentManifest)
    .then((response) => {
        console.log('Deployment created:', response.body);
    })
    .catch((err) => {
        console.error('Error creating deployment:', err.response.body);
    });

// To update the deployment, modify the deployment manifest and use the patchNamespacedDeployment method:
function updatedDeploymentManifest() {
    const updatedDeploymentManifest = {
        metadata: {
          name: 'my-test-deployment',
        },
        spec: {
          replicas: 4, // Update the number of replicas
        },
    };
      
    k8sApi.patchNamespacedDeployment('my-test-deployment', 'barrier-default', updatedDeploymentManifest)
        .then((response) => {
            console.log('Deployment updated:', response.body);
        })
        .catch((err) => {
            console.error('Error updating deployment:', err.response.body);
        });
}
  
  