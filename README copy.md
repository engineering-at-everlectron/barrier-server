To build and maintain your project using Docker Compose and ensure that it runs on startup with Chromium displaying the web interface, follow these steps:

### 1. Set Up Buildroot

First, ensure Buildroot is configured to include Docker and Chromium.

1. **Select Docker and Chromium**:
   - Open Buildroot configuration menu:
     ```sh
     make menuconfig
     ```
   - Navigate and select Docker:
     ```
     Target packages -> System tools -> docker-engine
     ```
   - Navigate and select Chromium:
     ```
     Target packages -> Graphics -> Web browsers -> chromium
     ```

### 2. Prepare Docker Compose File

Ensure you have a `docker-compose.yml` file ready for your application. For example:

```yaml
version: '3.8'

services:
  pwa-frontend:
    image: your-frontend-image
    ports:
      - "80:80"
    depends_on:
      - microservice1
      - microservice2
      - microservice3

  microservice1:
    image: your-microservice1-image
    ports:
      - "3000:3000"
    network_mode: "host"

  microservice2:
    image: your-microservice2-image
    ports:
      - "3001:3001"

  microservice3:
    image: your-microservice3-image
    ports:
      - "3002:3002"
```

### 3. Create Systemd Service for Docker Compose

Create a systemd service file to pull the Docker Compose file and run it on startup.

#### Docker Compose Pull Script

Create a script to pull and run the Docker Compose setup. Place it in your `package` directory:

`package/my_project/docker-compose-start.sh`:
```sh
#!/bin/sh

# Ensure Docker is started
systemctl start docker

# Change to the directory where your docker-compose.yml is located
cd /path/to/your/docker-compose

# Pull the latest images and start the services
docker-compose pull
docker-compose up -d
```
Make the script executable:
```sh
chmod +x package/my_project/docker-compose-start.sh
```

#### Systemd Service File

Create a systemd service file to run your script on startup:

`package/my_project/docker-compose.service`:
```ini
[Unit]
Description=Docker Compose Application
Requires=docker.service
After=docker.service

[Service]
Type=oneshot
ExecStart=/path/to/docker-compose-start.sh
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```

### 4. Configure Chromium to Open on Boot

Create a systemd service file to launch Chromium in kiosk mode pointing to your PWA:

`package/my_project/chromium.service`:
```ini
[Unit]
Description=Chromium Web Browser
After=docker-compose.service

[Service]
ExecStart=/usr/bin/chromium --no-sandbox --disable-infobars --kiosk http://localhost
Restart=always
Environment=HOME=/root

[Install]
WantedBy=multi-user.target
```

### 5. Install and Enable Services

Modify your `my_project.mk` to install these scripts and service files:

`package/my_project/my_project.mk`:
```sh
define MY_PROJECT_INSTALL_INIT_CMDS
    $(INSTALL) -D -m 0755 package/my_project/docker-compose-start.sh $(TARGET_DIR)/path/to/docker-compose-start.sh
    $(INSTALL) -D -m 0644 package/my_project/docker-compose.service $(TARGET_DIR)/lib/systemd/system/docker-compose.service
    $(INSTALL) -D -m 0644 package/my_project/chromium.service $(TARGET_DIR)/lib/systemd/system/chromium.service

    ln -sf /lib/systemd/system/docker-compose.service $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/docker-compose.service
    ln -sf /lib/systemd/system/chromium.service $(TARGET_DIR)/etc/systemd/system/multi-user.target.wants/chromium.service
endef

$(eval $(generic-package))
```

### 6. Enable Services in Buildroot Configuration

Ensure that your custom package is enabled in Buildroot:

- Open Buildroot configuration menu:
  ```sh
  make menuconfig
  ```
- Navigate to your package and enable it:
  ```
  Target packages -> My Project
  ```

### 7. Build and Deploy

After configuring everything, build your custom Linux image:
```sh
make
```
Flash the generated image to your Rockchip SoC. Upon boot, Docker Compose will pull the latest images and run your services, and Chromium will open in kiosk mode, displaying your PWA running on port 80.

### Summary

1. **Configure Buildroot** to include Docker and Chromium.
2. **Prepare Docker Compose File** for your services.
3. **Create startup scripts and systemd service files** to manage Docker Compose and Chromium.
4. **Modify Buildroot package** to include and enable these scripts and services.
5. **Build and deploy** your custom Linux image.

By following these steps, your embedded Linux system will automatically pull and run your Docker Compose setup on startup, and Chromium will display your web app in kiosk mode.