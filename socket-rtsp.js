const app = require('express')(),
  server = require('http').Server(app),
  io = require('socket.io')(server),
  rtsp = require('rtsp-ffmpeg');
server.listen(6147);

var uri = 'rtsp://admin:G@br13l@192.168.1.108:554/cam/realmonitor?channel=1&subtype=1',
  stream = new rtsp.FFMpeg({input: uri});

io.on('connection', function(socket) {
  console.log('Connection');
  var pipeStream = function(data) {
    socket.emit('data', data.toString('base64'));
  };
  stream.on('data', data => {
    console.log('Data', data);
    pipeStream(data);
  });
  socket.on('disconnect', function() {
    console.log('Disconnect');
    stream.removeListener('data', pipeStream);
  });
});

app.get('/', function (req, res) {
  console.log('Get');
  res.sendFile(__dirname + '/index.html');
});