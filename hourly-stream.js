const ffmpeg = require('fluent-ffmpeg');

const rtspToHLS = () => {
    console.log('RTSP to HLS stream start... 1 hour');
    ffmpeg('rtsp://barrierDemo:barrierDemo@barrier-test-ddns.freemyip.com:554/cam/realmonitor?channel=1&subtype=1')
      .audioCodec('aac')
      .audioBitrate('160000')
      .audioChannels(2)
      .size('800x600')
      .videoCodec('mjpeg')
      .videoBitrate(500000)
      .addOption('-hls_time', 5)
      .addOption('-hls_list_size', 5)
      .addOption('-start_number', 5)
      .duration(3600)
      .save(__dirname + '/mystream.m3u8')
      .on('progress', function(info) {
        console.log('progress ' + info.percent + '%');
      })
      .on('end', function() {
        console.log('End event..');
      })
      .on('codecData', function(data) {
        console.log('Input is ' + data.audio + ' audio ' +
          'with ' + data.video + ' video');
      })
      .on('error', function(err) {
        console.log('an error happened: ' + err.message);
      });
  
      // startrtspToHLS.kill(); 
};

rtspToHLS();