// SSL Integration
const rtspRelay = require('rtsp-relay');
const express = require('express');
const https = require('https');
const fs = require('fs');
const path = require('path');

// const key = fs.readFileSync(path.join(__dirname, 'cert', 'key.pem'));
// const cert = fs.readFileSync(path.join(__dirname, 'cert', 'cert.pem'));
// const cert = fs.readFileSync('cert.pem', 'utf8');
const key = fs.readFileSync('../cert/CA/localhost/localhost.decrypted.key');
const cert = fs.readFileSync('../cert/CA/localhost/localhost.crt');

const app = express();
const server = https.createServer({ key, cert }, app);
const cors = require('cors');
const { proxy, scriptUrl } = rtspRelay(app, server);

const handler = proxy({
  url: `rtsp://barrierDemo:barrierDemo@192.168.1.108:554/cam/realmonitor?channel=1&subtype=1`,
  // if your RTSP stream need credentials, include them in the URL as above
  verbose: false,
  additionalFlags: ['-q', '1']
});

const hikHandler = proxy({
  url: `rtsp://barrierDemo:b@rrierDemo86@192.168.1.64:554/Streaming/Channels/101`,
  // if your RTSP stream need credentials, include them in the URL as above
  verbose: false,
  additionalFlags: ['-q', '1']
});

// Add headers before the routes are defined
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:8100');
  // res.setHeader('Access-Control-Allow-Origin', 'https://www.barrier.everlectron.com');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Pass to next layer of middleware
  next();
});

// the endpoint our RTSP uses
app.ws('/api/stream', handler);

// the endpoint our RTSP uses
app.ws('/api/stream/hik', hikHandler);

app.ws('/api/stream/hikvision/:cameraRTSP', (ws, req) => {
  console.log('New Hik Stream');

  proxy({
    url: `rtsp://${req.params.cameraRTSP}/Streaming/Channels/101`,
    verbose: false,
    additionalFlags: ['-q', '1']
  })(ws),
  ws.on('message', data => {
    client.incomingPacket(data, false);
    console.log('New Message');

    //Emit message to request-detail
  })
});

app.ws('/api/stream/dahua/:cameraRTSP', (ws, req) => {
  console.log('New Dahua Stream');
  proxy({
    url: `rtsp://${req.params.cameraRTSP}/cam/realmonitor?channel=1&subtype=1`,
    verbose: false,
    additionalFlags: ['-q', '1']
  })(ws),
  ws.on('message', data => {
    client.incomingPacket(data, false);
    console.log('New Message');

    //Emit message to request-detail
  })
});

// this is an example html page to view the stream
app.get('/', (req, res) => {
  console.log('Get Page');
  res.send(`
    <h1>CCTV Feed</h1>
    <canvas id='canvas'></canvas>
    <canvas id='hik'></canvas>

    <script src='${scriptUrl}'></script>
    <script>
      loadPlayer({
        url: 'wss://' + location.host + '/api/stream',
        canvas: document.getElementById('canvas')
      });
      loadPlayer({
        url: 'wss://' + location.host + '/api/stream/hik',
        canvas: document.getElementById('hik')
      });
    </script>
  `)
});

app.get('/isOnline', (req, res) => {
  console.log('Online Check');
  res.status(200).json({ isOnline: true });
});

server.listen(3343, () => {
  console.log(`Secure SSL/TSL Server Started on port 3343!`);
});