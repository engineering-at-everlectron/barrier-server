// SSL Integration
const express = require('express');
// const https = require('https');
const http = require('http');

const fs = require('fs');
const path = require('path');
const ffmpeg = require('fluent-ffmpeg');

// import { createFFmpeg, fetchFile } from '@ffmpeg/ffmpeg';

// const key = fs.readFileSync(path.join(__dirname, 'cert', 'key.pem'));
// const cert = fs.readFileSync(path.join(__dirname, 'cert', 'cert.pem'));
// const cert = fs.readFileSync('cert.pem', 'utf8');
// const key = fs.readFileSync('./cert/CA/localhost/localhost.decrypted.key');
// const cert = fs.readFileSync('./cert/CA/localhost/localhost.crt');

const app = express();
const server = http.createServer(app);
const cors = require('cors');

const rtspToMP4 = () => {
  console.log('RTSP to MP4 conversion start...');
  const startRtspToMP4 = ffmpeg('rtsp://192.168.1.108:8554/mjpeg/1')
    .inputOptions([
      '-f', 'lavfi','-i', 'anullsrc', '-rtsp_transport', 'udp'
    ])
    //.duration(134.5) //In seconds to record. The process will end thereafter
    .outputOptions([
      '-force_key_frames', 'expr:gte(t,n_forced*2)', 
      '-vf', 'scale=1920:1080', '-reorder_queue_size', 
      '4000', '-max_delay', '10000000', '-vcodec', 'libx264',
      '-b:v', '4500k', '-pix_fmt', 'yuv420p', '-f', 'mp4'
    ])
    .save(__dirname + '/recordings/out.mp4')
    .duration(60)
    .on('progress', function(info) {
      console.log('progress ' + info.percent + '%');
    })
    .on('codecData', function(data) {
      console.log('Input is ' + data.audio + ' audio ' +
        'with ' + data.video + ' video');
    })
    .on('end', function() {
      console.log('End event..');
    })
    .on('error', function(err) {
      console.log('an error happened: ' + err.message);
    });

    // startRtspToMP4.kill(); //Manual FFMPEG Kill running process
};

const espRtspToMP4 = () => {
  console.log('RTSP to MP4 conversion start...');
  const startRtspToMP4 = ffmpeg('rtsp://192.168.1.108:8554/mjpeg/1')
    .inputOptions([
      '-rtsp_transport', 'udp'
    ])
    //.duration(134.5) //In seconds to record. The process will end thereafter
    .outputOptions([
      '-force_key_frames', 'expr:gte(t,n_forced*2)', 
      '-vf', 'scale=800:600', '-reorder_queue_size', 
      '4000', '-max_delay', '10000000', '-vcodec', 'mjpeg',
      '-b:v', '500k', '-pix_fmt', 'yuv420p', '-f', 'mp4'
    ])
    .save(__dirname + '/recordings/out.mp4')
    .duration(60)
    .on('progress', function(info) {
      console.log('progress ' + info.percent + '%');
    })
    .on('codecData', function(data) {
      console.log('Input is ' + data.audio + ' audio ' +
        'with ' + data.video + ' video');
    })
    .on('end', function() {
      console.log('End event..');
    })
    .on('error', function(err) {
      console.log('an error happened: ' + err.message);
    });

    // startRtspToMP4.kill(); //Manual FFMPEG Kill running process
};

// const mp4ToGIF = () => {
//   console.log('MP4 to GIF convert start...');
//   ffmpeg('rtsp://192.168.1.108:8554/mjpeg/1')
//     .inputOptions([
//       '-f', 'lavfi','-i', 'anullsrc', '-rtsp_transport', 'udp'
//     ])
//     .outputOptions([
//       '-force_key_frames', 'expr:gte(t,n_forced*2)', 
//       '-vf', 'scale=800:600', '-reorder_queue_size', 
//       '4000', '-max_delay', '10000000', '-vcodec', 'mjpeg',
//       '-b:v', '4500k', '-pix_fmt', 'yuv420p', '-f', 'flv'
//     ])
//     .save(__dirname + '/gifs/test.gif')
//     .on('progress', function(info) {
//       console.log('progress ' + info.percent + '%');
//     })
//     .on('end', function() {
//       console.log('End event..');
//     })
//     .on('error', function(err) {
//       console.log('an error happened: ' + err.message);
//     });
// };

const rtspToRTMP = () => {
  console.log('RTSP to RTMP stream start...');
  const rtspToRTMP = ffmpeg('rtsp://192.168.1.108:8554/mjpeg/1')
    .inputOptions([
      '-f', 'lavfi','-i', 'anullsrc', '-rtsp_transport', 'udp'
    ])
    .outputOptions([
      '-force_key_frames', 'expr:gte(t,n_forced*2)', 
      '-vf', 'scale=1920:1080', '-reorder_queue_size', 
      '4000', '-max_delay', '10000000', '-vcodec', 'libx264',
      '-b:v', '4500k', '-pix_fmt', 'yuv420p', '-f', 'flv'
    ])
    .save('rtmp://a.rtmp.youtube.com/live2/dhr6-2jqm-rdz4-4k7q-7hbt')
    .on('progress', function(info) {
      console.log('progress ' + info.percent + '%');
    })
    
    .on('codecData', function(data) {
      console.log('Input is ' + data.audio + ' audio ' +
        'with ' + data.video + ' video');
    })
    .on('end', function() {
      console.log('End event..');
    })
    .on('error', function(err) {
      console.log('an error happened: ' + err.message);
    });

    //rtspToRTMP.kill(); 
};

const rtspToHLS = () => {
  console.log('RTSP to HLS stream start...');
  ffmpeg('rtsp://192.168.1.108:8554/mjpeg/1')
    .audioCodec('aac')
    .audioBitrate('160000')
    .audioChannels(2)
    .size('800x600')
    .videoCodec('mjpeg')
    .videoBitrate(500000)
    .addOption('-hls_time', 5)
    .addOption('-hls_list_size', 5)
    .addOption('-start_number', 5)
    .save(__dirname + '/recordings/mystream.m3u8')
    .on('progress', function(info) {
      console.log('progress ' + info.percent + '%');
    })
    .on('end', function() {
      console.log('End event..');
    })
    .on('codecData', function(data) {
      console.log('Input is ' + data.audio + ' audio ' +
        'with ' + data.video + ' video');
    })
    .on('error', function(err) {
      console.log('an error happened: ' + err.message);
    });

    // startrtspToHLS.kill(); 
};


// Add headers before the routes are defined
app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');
  // res.setHeader('Access-Control-Allow-Origin', 'https://www.barrier.technology');
  
  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  
  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  
  // Pass to next layer of middleware
  next();
});

//setting middleware
app.use('/footage', express.static(__dirname + '/recordings')); //Serves resources from recordings folder

// this is an example html page to view the stream
app.get('/', (req, res) => {
  console.log('Get Page');

  // rtspToRTMP();

  res.send(`
    <h1>Hello Friend</h1>
  `)
});

// this is an example html page to view the stream
app.get('/hls', (req, res) => {
  console.log('Get Page');

  rtspToHLS();

  res.send(`
    <h1>RTSP to HLS with Fluent-FFMPEG</h1>
  `)
});

app.get('/mp4', (req, res) => {
  console.log('Get Page');

  espRtspToMP4();

  res.send(`
    <h1>RTSP to MP4 with Fluent-FFMPEG</h1>
  `)
});

// app.get('/gif', (req, res) => {
//   console.log('Get Page');

//   mp4ToGIF();

//   res.send(`
//     <h1>RTSP to GIF with Fluent-FFMPEG</h1>
//   `)
// });

app.get('/isOnline', (req, res) => {
  console.log('Online Check');
  res.status(200).json({ isOnline: true });
});

app.get('/stop', (req, res) => {
  console.log('Stop FFMPEG');
  
  res.status(200).json({ isStopped: true });
});

server.listen(3005, () => {
  console.log(`Secure SSL/TSL Server Started on port 3005!`);
});