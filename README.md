
# Barrier Technician Guide

The purpose of this document is to layout the steps required in order to setup local infrastructure at the facility and to add camera's to it.


## Setting Up Local Server

The local camera server is responsible for the decoding of the video stream that comes from the IP camera on the local network.

## Device Requirements

[Download NodeJS](https://nodejs.org)

[Download VLC](https://videolan.org)

[Download OpenSSL](https://openssl.org)

## NodeJS Requirements

PM2 is a process manager for NodeJS we'll need this later.

```bash
    npm install -g pm2
```

We will need an HTTP server to run our compiled app locally.

```bash
    npm install -g http-server
```

Clone the camera-server repo to a folder in the devices documents.

```bash
    git clone https://everlectron@bitbucket.org/engineering-at-everlectron/barrier-server.git
```

Change directory into the barrier-server folder and test server.

```bash
    node stream.js
```
## SSL Certificate

Step 1: In order to run the local server securely you will need a self generated SSL certificate to attach to the node app. For this we will work in the /cert directory.

```bash
    cd /cert
    mkdir CA
    cd CA
    openssl genrsa -out CA.key -des3 2048
```

The script above will generate a root CA certificate. It will also generate a private key and request a passphrase for the key. Use "hostinglocal" as the passphrase.

Next, we will generate a root CA certificate using the key generated, that will be valid for ten years in our case. The passphrase for the key and certificate info will be requested.

```bash
    openssl req -x509 -sha256 -new -nodes -days 3650 -key CA.key -out CA.pem
```

Step 2: Generating a certificate

Now have created the CA key and CA certificate. It is possible to sign SSL certificates since we already created CA.

Next, in the cert/CA directory create a new directory, localhost. Inside localhost create a new file, localhost.ext.

```bash 
    mkdir localhost
    cd localhost
    touch localhost.ext
```

The information that needs to be written into the signed SSL certificate will be contained in this localhost.ext file.

```bash
    authorityKeyIdentifier = keyid,issuer
    basicConstraints = CA:FALSE
    keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
    subjectAltName = @alt_names

    [alt_names]
    DNS.1 = localhost
    IP.1 = 127.0.0.1
    <IP.n> = <Add additional localhost address here>
```

Next will be to generate a key and use the key to generate a CSR (Certificate Signing Request) using the command below.

```bash
    openssl genrsa -out localhost.key -des3 2048
```

The command will generate the localhost private key, and the passphrase will be requested for the key, and the user will be asked to confirm it again.

Next will be to generate CSR using the key, and then the passphrase create above will be requested. Any other details requested can be left as default or keyed in as appropriate.

Note the challenge password requested; one can enter anything.

Use the below command to generate the CSR:

```bash
    openssl req -new -key localhost.key -out localhost.csr
```

Now with this CSR, we can request the CA to sign a certificate as below. Note that the paths for CA.key and CA.pem files are dependent on where the user is running commands from. In this case, the commands below are ran from /cert/CA/localhost.

```bash
    openssl x509 -req -in localhost.csr -CA ../CA.pem -CAkey ../CA.key -CAcreateserial -days 3650 -sha256 -extfile localhost.ext -out localhost.crt
```

This command takes in the CSR (localhost.csr), the CA certificate (CA.pem and CA.key), and the certificate extensions file (localhost.ext). Those inputs generate a localhost.crt certificate file, valid for ten years.

The server will need the localhost.crt certificate file, and the decrypted key since our localhost.key is in encrypted form.

We will need to decrypt the localhost.key and store that file too as below:

```bash
    openssl rsa -in localhost.key -out localhost.decrypted.key
```

With that done you can re-run the node server (stream.js) in the root directory and test it using https://localhost:3343 in the browser. Emphasis on https. This should work because the required SSL certificates and keys are present.

If you had any issues have a look at this link:
[SSL Cert Guide](https://www.section.io/engineering-education/how-to-get-ssl-https-for-localhost/)