const express = require('express');
const http = require('http');
const app = express();

const server = http.createServer(app);
const cors = require('cors');

// Add headers before the routes are defined
app.use(function (req, res, next) {
    // Website you wish to allow to connect
    // res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Origin', 'https://www.barrier.technology');
    
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', '*');
    
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    
    // Pass to next layer of middleware
    next();
});

app.get('/', (req, res) => {
    console.log('Get Root Page');
  
    res.send(`
      <h1>Serving to you, from Barrier HQ!</h1>
    `)
});
  
//setting middleware
app.use('/footage', express.static(__dirname + '/recordings')); //Serves resources from recordings folder

app.get('/isOnline', (req, res) => {
    console.log('Online Check');
    res.status(200).json({ isOnline: true });
});

server.listen(4000, () => {
    console.log(`Secure SSL/TSL Server Started on port 4000!`);
});