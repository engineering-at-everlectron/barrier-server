const fs = require('fs');
const path = require('path');
const base64Img = require('base64-img');

const inputFolder = 'captures';
const outputFolder = 'base64_images';
const axios = require('axios');


// Ensure the output folder exists
if (!fs.existsSync(outputFolder)) {
  fs.mkdirSync(outputFolder);
}

async function postBase64ImageWithOtherDataAndToken(filePath, apiUrl) {
  console.log('Captioning...');
    try {
      // Read the base64 data from the txt file
      const base64Data = fs.readFileSync(filePath, 'utf-8').trim().split(',')[1];
      const authToken = 'ya29.c.c0AY_VpZi5_iyVvtbiTJm-WdJp92pWPCETxAiGRDbic2zv2yA_VrXXgAW-V9Nl8S8DeuPdYH3V9EUpVhJ9YJRy3RX7Q-Emm3ZZpv_YfGoUtHYtRCPCd0-VImyKp2jalxtb1gUgkC0yE6FBofZLIV6mKOIucfvCCQMVb_Hus8RleRj8s330EN3iTbRihn6mVon1aGloI6bxb3juUAN0e4UGc6ZPdTco2qsGDsH0zFtmztGc9xqWCVho2uOV5o0gVmr5dN7zPEJYnyt38O_Zqd6jS7G_vDOe5-0RW2KphOp_1CQNAKbk_txMQT5FqVhN72-XhqxW1iWUIAELCk5hHncC_15O3VIQsYlWc0GIJjFkt6qBHhsqgzSN6kVXBSN5ocwDMakzIAL399Dfscegtx5ug-mYvI-aWp3qf1FaarycXUn6wY7-96e6qF4Rrmuhb9aW1yJwp8narVdjd3dcXbfs6dO22w3x9r-Qz_XIsSfwBpubZjpzR7YyJapIQ0Z-M-3t-ZBMi4FJ236Fwogvt_cv45zJrYkMIWtU67nSZFwyOYQqI3UlqzUljxkbRJaI-l0kWvywu-y8g11_iBrW4RbunRos2f2zfhpIk6hcjYlW9_uFt7rlt4sr-qk-6YhWBslY5jc6tB4gQ_r0Y6ecv20rmld4qZlb3vJosI3JplanhFsXjV66QUkVy1zlV3wd2dknWXBez7kgQb5f5Z_d7g6cidbt1U_eUFjcyw0mxOo06R0Bblq3ojlJmh9Q489ot_nI02e-FgeIotMmzlavclYVk2wqWrsr2X2OUfW0gb85r955Omx-e4yXaqX3cuYyrFedlOtvcY0nlrqBzWyc42xb73i0mp2bm6OXJsq-hrh-p64fgRbnc580OF0Ba9cefQ99XOn3h1gZ1lyJFm5r_myf-nX1kmv-6ciqfyxwXqjbsX4ujwe2w-rSqbMltWVl8mM0pIaYom98Z__VRi48Rq73mBo85UzQVWnmbZ0sOmg1hR3XlgbtXI_qYvx';
  
      // Make a POST request to the specified API endpoint with base64 image, other data, and Bearer token
      const response = await axios.post(apiUrl, {
        instances: [
            {
                image: {
                    bytesBase64Encoded: base64Data
                }
            }
        ],
        parameters: {
            sampleCount: 1,
            language: "en"
        },
      }, {
        headers: {
          'Content-Type': 'application/json; charset=utf-8', 
          'Authorization': `Bearer ${authToken}`,
        },
      });
  
      console.log('Image and other data posted successfully:', response.data);
    } catch (error) {
      console.log('Error: ', error);
      console.error('Error posting image and other data:', error.message);
    }
}

// Watch the input folder for new images
fs.watch(inputFolder, (event, filename) => {
  if (event === 'rename' && filename) {
    const inputImagePath = path.join(inputFolder, filename);
    const outputImagePath = path.join(outputFolder, filename + '.txt');

    // Convert the image to base64
    base64Img.base64(inputImagePath, (err, data) => {
      if (err) {
        console.error(`Error converting ${filename} to base64:`, err);
      } else {
        // Save the base64 data to a text file
        fs.writeFileSync(outputImagePath, data);
        console.log(`${filename} converted to base64 and saved to ${outputImagePath}`);

        //Run Imagen Request
        // Example usage:
        // const filePath = 'base64_images/sampleImage.txt';

        const apiUrl = 'https://us-central1-aiplatform.googleapis.com/v1/projects/barrier-34a55/locations/us-central1/publishers/google/models/imagetext:predict'; // Replace with your API endpoint

        postBase64ImageWithOtherDataAndToken(outputImagePath, apiUrl);
      }
    });
  }
});

console.log('Listening for new images in', inputFolder);
