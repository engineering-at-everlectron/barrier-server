const path = require('path');
const express = require('express');
const WebSocket = require('ws');

const app = express();
const bodyParser = require("body-parser");
const { Socket } = require('socket.io');

const Stream = require('node-rtsp-stream');

//MessageBird
const messagebird = require('messagebird')('TF2ucxrWql9LGeBtcANSDEug2');

//Here we are configuring express to use body-parser as middle-ware.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const HTTP_PORT = parseInt(process.env.PORT) || 8080;
const WS_PORT  = 443;
const WS_PORT_TWO  = 8889;

// app.get('/client',(req,res) => 
//     res.sendFile(path.resolve(__dirname, './client.html'))
// );

// app.get('/facility',(req,res) => 
//     res.sendFile(path.resolve(__dirname, './facility.html'))
// );



app.post('/new-stream', (req, res) => {
    console.log('New Stream');

    // let message = req.body.message;
    // let cell = req.body.cell;
    // console.log('Message: ', message);
    // console.log('Cell: ', cell);

    const stream = new Stream({
        name: 'Dahua Cam',
        streamUrl: 'rtsp://admin:G@br13l86@192.168.1.108:554/cam/realmonitor?channel=1&subtype=1',
        wsPort: 9999,
        ffmpegOptions: { // options ffmpeg flags
          '-stats': '', // an option with no neccessary value uses a blank string
          '-r': 30 // options with required values specify the value after the key
        }
    });

    stream.start();
});

app.post('/new-message', (req, res) => {
    console.log('New message');

    let message = req.body.message;
    let cell = req.body.cell;
    console.log('Message: ', message);
    console.log('Cell: ', cell);

    var params = {
        'recipients': [
            cell
        ],
        'body': message,
        'language': 'en-gb',
        'voice': 'female'
    };
      
    messagebird.voice_messages.create(params, function (err, data) {
        if (err) {
            return console.log(err);
        }
        res.send('Voice message request received successfully.');
        console.log("Voice Message Sent Successfully");
    });

});

const wsServer = new WebSocket.Server({port: WS_PORT}, ()=> console.log(`WS Server is listening at ${WS_PORT}`));
const wsServerTwo = new WebSocket.Server({port: WS_PORT_TWO}, ()=> console.log(`WS Server Two is listening at ${WS_PORT_TWO}`));

let connectedClients = [];
let connectedClientsTwo = [];

wsServer.on('connection', (ws, req)=>{
    console.log('Socket server one connection.');
    connectedClients.push(ws);

    ws.on('message', data => {
        connectedClients.forEach((ws,i)=>{
            if(ws.readyState === ws.OPEN){
                ws.send(data);
            }else{
                connectedClients.splice(i ,1);
            }
        })
    });
});

wsServerTwo.on('connection', (ws, req)=> {
    console.log('Socket server two connection.');
    connectedClientsTwo.push(ws);

    ws.on('message', data => {
        connectedClientsTwo.forEach((ws,i)=>{
            if(ws.readyState === ws.OPEN){
                ws.send(data);
            }else{
                connectedClientsTwo.splice(i ,1);
            }
        })
    });
});

//Socket.IO
const server = require('http').createServer();
const io = require('socket.io')(server);
io.on('connection', client => {
    console.log('Socket server new client connected.');
    client.on('event', data => { 
        console.log('Client event: ', data);
    });

    client.on('Guard Request', (data) => {
        console.log('New Guard Request');
        client.emit('Guard Request', data);
    }); 

    client.on('disconnect', () => {
        console.log('Client disconnected.');
    });
});

server.listen(3000, () => console.log("SocketIO server listening at port 3000"));
app.listen(HTTP_PORT, ()=> console.log(`HTTP server listening at ${HTTP_PORT}`));
